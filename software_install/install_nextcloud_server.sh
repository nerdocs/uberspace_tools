#!/bin/sh
# Installs a nexctcloud server instance on your uberspace asteroid
#
# If you want a custom domain (not $USER.uber.space), you have to point your domain 
# to your userspace BEFORE you run this script.
#  
# Run this script using:
#   wget https://gitlab.com/nerdocs/uberspace_tools/-/raw/main/software_install/install_nextcloud_server.sh
#   ./install_nextcloud_server.sh <your.domain>
#
# The script will ask you for the admin username/password, or you can set it before using the shell variables
# NEXTCLOUD_ADMIN_USER and NEXTCLOUD_ADMIN_PASS


die() {
    echo "ERROR: ${1}"
    exit 1
}

usage() {
  echo Usage: install_nextcloud_server.sh <domain>
}

log() {
    echo " * ${1}"
}

if [ "${1}" == "" ]; then
  usage
fi

log "Set up domain..."
DOMAIN="${1}"
NEXTCLOUD_DIR=/var/www/virtual/${USER}/$DOMAIN
uberspace web domain add ${DOMAIN}

cat << EOF >> ~/etc/php.d/opcache.ini
opcache.enable=1
opcache.enable_cli=1
opcache.interned_strings_buffer=16
opcache.max_accelerated_files=10000
opcache.memory_consumption=128
opcache.save_comments=1
opcache.revalidate_freq=1
EOF

cat << EOF >> ~/etc/php.d/apcu.ini
apc.enable_cli=1
EOF

cat << EOF >> ~/etc/php.d/memory_limit.ini
memory_limit=512M
EOF

cat << EOF >> ~/etc/php.d/output_buffering.ini
output_buffering=off
EOF

uberspace tools restart php

log "Downloading Nextcloud server..."
mkdir -p ${NEXTCLOUD_DIR}
cd ${NEXTCLOUD_DIR}
rm nocontent.html
curl https://download.nextcloud.com/server/releases/latest.tar.bz2 | tar -xjf - --strip-components=1

log "Creating database..."
mysql --verbose --execute="CREATE DATABASE ${USER}_nextcloud"

log "Set up Nextcloud/admin user"
while [ ${NEXTCLOUD_ADMIN_USER} != "" ]; do
  read -p="Admin user: " NEXTCLOUD_ADMIN_USER
done
while [ ${NEXTCLOUD_ADMIN_PASS} != "" ]; do
  read -p="Admin password" NEXTCLOUD_ADMIN_PASS
done

MYSQL_PASSWORD="$(my_print_defaults client|grep password|sed 's/--password=\(.*\)/\1/')"
php ${NEXTCLOUD_DIR}/occ maintenance:install --admin-user="${NEXTCLOUD_ADMIN_USER}" --admin-pass="${NEXTCLOUD_ADMIN_PASS}" --database='mysql' --database-name="${USER}_nextcloud"  --database-user="${USER}" --database-pass="${MYSQL_PASSWORD}" --data-dir="${HOME}/nextcloud_data"

log "Set trusted domain..."
php ${NEXTCLOUD_DIR}/occ config:system:set trusted_domains 0 --value="${DOMAIN}"
php ${NEXTCLOUD_DIR}/occ config:system:set overwrite.cli.url --value="https://${DOMAIN}"

log "Add shortcut to log files..."
ln -s ~/nextcloud_data/nextcloud.log ~/logs/nextcloud.log
ln -s ~/nextcloud_data/updater.log ~/logs/nextcloud-updater.log

log "Set up mail system..."
php ${NEXTCLOUD_DIR}/occ config:system:set mail_domain --value="uber.space"
php ${NEXTCLOUD_DIR}/occ config:system:set mail_from_address --value="$USER"
php ${NEXTCLOUD_DIR}/occ config:system:set mail_smtpmode --value="sendmail"
php ${NEXTCLOUD_DIR}/occ config:system:set mail_sendmailmode --value="pipe"

php ${NEXTCLOUD_DIR}/occ config:system:set htaccess.RewriteBase --value='/'
php ${NEXTCLOUD_DIR}/occ maintenance:update:htaccess

log "Installing crontab..."
echo '*/5  *  *  *  * sleep $(( 1 + RANDOM \% 60 )) ; php -f ${NEXTCLOUD_DIR}/cron.php > $HOME/logs/nextcloud-cron.log 2>&1' |crontab
php ${NEXTCLOUD_DIR}/occ background:cron

log "Activate memcache"
php ${NEXTCLOUD_DIR}/occ config:system:set memcache.local --value='\OC\Memcache\APCu'

while [ ${DEFAULT_PHONE_REGION} != "" ]; do
  read -p "Set default phone region (DE): " DEFAULT_PHONE_REGION
done
php ${NEXTCLOUD_DIR}/occ config:system:set default_phone_region --value="${DEFAULT_PHONE_REGION}"

log "Optimize database..."
php ${NEXTCLOUD_DIR}/occ db:add-missing-indices --no-interaction
php ${NEXTCLOUD_DIR}/occ db:add-missing-columns --no-interaction
php ${NEXTCLOUD_DIR}/occ db:add-missing-primary-keys --no-interaction
php ${NEXTCLOUD_DIR}/occ db:convert-filecache-bigint --no-interaction

log "Install an update script..."
cat << EOF >> ~/bin/nextcloud-update
#!/usr/bin/env bash
## Updater automatically works in maintenance:mode.
## Use the Uberspace backup system for files and database if you need to roll back.
## The Nextcloud updater creates backups only to safe base and app code data and config files
## so it takes ressources you might need for your productive data.
## Deactivate NC-updater Backups with --no-backup
php ${NEXTCLOUD_DIR}/updater/updater.phar -vv --no-backup --no-interaction

## database optimisations
php ${NEXTCLOUD_DIR}/occ db:add-missing-primary-keys --no-interaction
php ${NEXTCLOUD_DIR}/occ db:add-missing-columns --no-interaction
php ${NEXTCLOUD_DIR}/occ db:add-missing-indices --no-interaction
php ${NEXTCLOUD_DIR}/occ db:convert-filecache-bigint --no-interaction

php ${NEXTCLOUD_DIR}/occ app:update --all
## App updates may require additional steps to be done by the `upgrade` command
php ${NEXTCLOUD_DIR}/occ upgrade
/usr/sbin/restorecon -R ${NEXTCLOUD_DIR}

## FYI: If that file exist...
if test -f ~/etc/services.d/notify_push.ini
then supervisorctl restart notify_push
fi
EOF


echo "Please don't forget to edit ~/html/config/config.php"